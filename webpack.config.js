const path = require('path');

module.exports = {
  resolve: {
    alias: {
      Teaching: path.resolve(__dirname, 'src/progress_teaching/'),
      Research: path.resolve(__dirname, 'src/progress_research/'),
      '@': path.resolve(__dirname, 'src/'),
      Common: path.resolve(__dirname, 'src/common/'),
    },
  },
};
