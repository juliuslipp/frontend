# Team Progress / Grundschule | Projekt Idee - Geschäftsmodell - MVP:Software as a Service

This projekt is a students bachelor project for the module 'Projekt Idee - Geschäftsmodell - MVP: Software as a Service' at the university of Hamburg, Germany. <br />
Progress is a platform for teachers and researchers for the documentation of lessons and also to increase the quality of the lessons. <br />
We support the educational research by providing first class data directly from the teachers. 

This is the repository for the frontend of our project.

Our frontend is mainly build with the [react framework](https://reactjs.org/).

## Requirements

Make sure you have a recent version of [Node.js](https://nodejs.org/en/) installed.

## Run

### `npm start`

Runs the project in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Build

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
The project is then ready to be deployed.
