module.exports = {
  root: true,
  env: {
    node: true,
    browser: true,
  },
  extends: ['airbnb', 'airbnb/hooks', 'prettier'],
  plugins: ['prettier'],
  parserOptions: {
    parser: 'babel-eslint',
  },
  rules: {
    'prettier/prettier': [
      process.env.NODE_ENV === 'production' ? 'error' : 'warn',
      { endOfLine: 'auto' },
    ],
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'warn',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'warn',
    camelcase: process.env.NODE_ENV === 'production' ? 'error' : 'warn',
    'no-param-reassign': process.env.NODE_ENV === 'production' ? 'error' : 'warn',
    'no-unused-vars': process.env.NODE_ENV === 'production' ? 'error' : 'warn',
    'spaced-comment': 'warn',
    'prefer-template': process.env.NODE_ENV === 'production' ? 'warn' : 'warn',
    'no-use-before-define': process.env.NODE_ENV === 'production' ? 'error' : 'warn',
    'react-hooks/exhaustive-deps': 'off',
    'react/jsx-one-expression-per-line': 'off',
    'react/prop-types': 'off',
  },
  settings: {
    'import/resolver': {
      alias: {
        map: [
          ['Research', './src/progress_research'],
          ['Teaching', './src/progress_teaching'],
          ['Common', './src/common'],
          ['@', './src'],
        ],
        extensions: ['.ts', '.js', '.jsx', '.json'],
      },
    },
  },
};
