import { Button } from '@material-ui/core';
import React from 'react';
import DocumentApiService from 'Common/services/DocumentApiService';
import { downloadObjectAsJson } from 'Common/services/Utils';

export default function ScreensDashboard() {
  const exportDb = () => {
    DocumentApiService.getDocumentList().then((res) => {
      downloadObjectAsJson(res, 'all_documents');
    });
  };

  return (
    <div>
      <Button color="primary" variant="outlined" onClick={exportDb}>
        Die Datenbank exportieren und herunterladen!{' '}
      </Button>
      <p>(könnte etwas dauern)</p>
    </div>
  );
}
