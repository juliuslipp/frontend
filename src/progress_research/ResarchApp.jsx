import React from 'react';
import { EventRounded, HomeRounded } from '@material-ui/icons';
import PropTypes from 'prop-types';

import Navbar from 'Common/components/navbar/Navbar';
import Footer from 'Common/components/footer/Footer';
import { WrappedPrivateRoute } from 'Common/components/PrivateRoute';
import NavbarItem from 'Common/components/navbar/item/Item';
import Calendar from 'Common/calendar/Calendar';
import ScreensDashboard from './dashboard/Dashboard';
import classes from './ResearchApp.module.scss';

export default function ScreensRoot(props) {
  // eslint-disable-next-line react/prop-types
  const { children, prefix, auth } = props;
  const wrapper = (properties) => <div className={classes.appContainer}>{properties.children}</div>;

  return (
    <>
      <Navbar prefix={prefix}>
        <NavbarItem title="Übersicht" icon={HomeRounded} link={`/${prefix}/dashboard`} />
        <NavbarItem title="Calendar" icon={EventRounded} link={`/${prefix}/calendar`} />
      </Navbar>
      <WrappedPrivateRoute
        path={`/${prefix}/dashboard`}
        exact
        component={ScreensDashboard}
        isAuthorized={auth}
        wrapper={wrapper}
      />
      <WrappedPrivateRoute
        wrapper={wrapper}
        isAuthorized={auth}
        component={Calendar}
        exact
        path={`/${prefix}/calendar`}
      />
      {children}
      <Footer />
    </>
  );
}

ScreensRoot.propTypes = {
  prefix: PropTypes.string.isRequired,
};
