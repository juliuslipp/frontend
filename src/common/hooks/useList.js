import { useState } from 'react';
import { v4 as uuid } from 'uuid';

export default function useList() {
  const [list, setList] = useState([]);

  const updateAtIndex = (data, index) => {
    setList((prev) => {
      const newState = [...prev];
      newState[index] = { ...prev[index], ...data };
      return newState;
    });
  };

  const deleteAtIndex = (index) => {
    setList((prevState) => {
      const newState = [...prevState];
      newState.splice(index, 1);
      return newState;
    });
  };

  const add = () => {
    setList((prev) => [...prev, { uniqueId: uuid() }]);
  };

  const getDataAsList = () =>
    list.reduce((acc, data) => {
      const newData = { ...data };
      delete newData.uniqueId;
      return Object.keys(newData).length > 0 ? [...acc, newData] : acc;
    }, []);

  return [list, getDataAsList, add, updateAtIndex, deleteAtIndex];
}
