import React, { useState } from 'react';
import { Button, FormHelperText, Grid, Link, TextField } from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';

import { handleHTTPErrorCodes } from 'Common/services/Utils';
import UserApiService from 'Common/services/UserApiService';

import classes from './Form.module.scss';

export default function ComponentLoginForm() {
  // const classes = useStyles();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  function validateForm() {
    return email.length > 0 && password.length > 0;
  }

  async function handleSubmit(event) {
    event.preventDefault();
    setErrorMessage('');
    UserApiService.login(email, password).catch(
      handleHTTPErrorCodes(
        {
          401: () => setErrorMessage('Email oder Passwort falsch!'),
        },
        () => setErrorMessage('Überprüfen Sie ihre Netzwerkverbindung!')
      )
    );
  }

  return (
    <form className={classes.form} onSubmit={handleSubmit} noValidate>
      <TextField
        error={!!errorMessage}
        id="email"
        label="E-Mail Adresse"
        variant="outlined"
        margin="normal"
        fullWidth
        type="email"
        value={email}
        autoComplete="email"
        onChange={(e) => setEmail(e.target.value)}
        placeholder="E-Mail"
      />
      <TextField
        error={!!errorMessage}
        id="password"
        label="Passwort"
        variant="outlined"
        margin="normal"
        fullWidth
        type="password"
        value={password}
        autoComplete="current-password"
        onChange={(e) => setPassword(e.target.value)}
        placeholder="Passwort"
      />
      {!!errorMessage && (
        <FormHelperText error id="component-error-text">
          {errorMessage}
        </FormHelperText>
      )}
      <Button
        variant="contained"
        type="submit"
        fullWidth
        color="primary"
        className={classes.button}
        disabled={!validateForm()}
      >
        Anmelden
      </Button>
      <Grid container>
        <Grid item>
          <Link to="/signup" variant="body2" component={RouterLink}>
            Kein Konto? Hier registrieren.
          </Link>
        </Grid>
      </Grid>
    </form>
  );
}
