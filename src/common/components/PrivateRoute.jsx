/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';

import { Route, Redirect } from 'react-router-dom';

// eslint-disable-next-line react/prop-types
export default function PrivateRoute({
  children,
  component,
  render,
  redirectionPath,
  isAuthorized,
  ...rest
}) {
  const Comp = component;
  const RedirectComp = () => <Redirect {...rest} to={redirectionPath} />;

  const renderFunction = (props) =>
    isAuthorized === true ? <Comp {...props} /> : <RedirectComp />;

  return !render ? (
    <Route {...rest} render={renderFunction} />
  ) : (
    <Route {...rest} render={(props) => (isAuthorized ? render(props) : <RedirectComp />)} />
  );
}

PrivateRoute.propTypes = {
  isAuthorized: PropTypes.bool.isRequired,
  redirectionPath: PropTypes.string,
  render: PropTypes.func,
};

PrivateRoute.defaultProps = {
  redirectionPath: '/login',
  render: undefined,
};

export function WrappedPrivateRoute(props) {
  const { component, wrapper, isAuthorized, ...clonedProps } = props;
  const Comp = component;
  const Wrapper = wrapper;
  return (
    <PrivateRoute
      {...clonedProps}
      isAuthorized={isAuthorized}
      render={(rest) => (
        <Wrapper>
          <Comp {...rest} />
        </Wrapper>
      )}
    />
  );
}

WrappedPrivateRoute.propTypes = {
  ...PrivateRoute.propTypes,
  wrapper: PropTypes.oneOfType([PropTypes.func, PropTypes.element]).isRequired,
};

WrappedPrivateRoute.defaultProps = {
  ...PrivateRoute.defaultProps,
};
