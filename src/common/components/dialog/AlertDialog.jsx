import React from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core';
import PropTypes from 'prop-types';

export default function AlertDialog(props) {
  const { acceptText, declineText, contentText, title, open, handleClose, danger } = props;
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
      <DialogContent dividers>
        <DialogContentText id="alert-dialog-description">{contentText}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button variant="text" onClick={() => handleClose(false)} color="primary" autoFocus>
          {declineText}
        </Button>
        <Button
          variant="text"
          onClick={() => handleClose(true)}
          color={danger ? 'secondary' : 'primary'}
        >
          {acceptText}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

AlertDialog.propTypes = {
  acceptText: PropTypes.string.isRequired,
  declineText: PropTypes.string.isRequired,
  contentText: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  danger: PropTypes.bool,
};

AlertDialog.defaultProps = {
  danger: false,
};
