import React, { useState } from 'react';
import classNames from 'classnames';
import { PowerSettingsNew } from '@material-ui/icons';

import UserApiService from 'Common/services/UserApiService';
import NavbarItem from 'Common/components/navbar/item/Item';
import classes from './Navbar.module.scss';
import NavbarLogo from './logo/Logo';

export default function Navbar(props) {
  const { children } = props;
  const [isCollapsed, setIsCollapsed] = useState(true);

  const childrenWithProps = React.Children.map(children, (child) => {
    if (React.isValidElement(child)) {
      return React.cloneElement(child, { collapsed: isCollapsed });
    }
    return child;
  });

  return (
    <nav
      onMouseEnter={() => setIsCollapsed(false)}
      onMouseLeave={() => setIsCollapsed(true)}
      className={classNames(classes.container, { [classes.containerCollapsed]: isCollapsed })}
    >
      <ul className={classes.list}>
        <NavbarLogo collapsed={isCollapsed} />
        {childrenWithProps}

        <NavbarItem
          title="Ausloggen"
          icon={PowerSettingsNew}
          collapsed={isCollapsed}
          warn
          onClick={UserApiService.logOut}
        />
      </ul>
    </nav>
  );
}
