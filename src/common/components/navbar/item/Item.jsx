import React from 'react';
import PropTypes from 'prop-types';
import { Link, useLocation } from 'react-router-dom';
import classNames from 'classnames';

import classes from './Item.module.scss';

export default function NavbarItem(props) {
  const { link, icon, title, collapsed, warn, onClick } = props;
  const Icon = icon;

  const location = useLocation();

  const handleClick = (e) => {
    if (onClick != null) {
      e.preventDefault();
      onClick();
    }
  };

  const isCurrentRoute = location.pathname === link;

  return (
    <li
      className={classNames(classes.listItem, {
        [classes.warning]: warn,
      })}
    >
      <Link
        to={link}
        className={classNames(classes.link, {
          [classes.highlight]: isCurrentRoute,
        })}
        onClick={handleClick}
      >
        {icon && <Icon className={classes.icon} />}
        <span className={classNames(classes.text, { [classes.textCollapsed]: collapsed })}>
          {title}
        </span>
      </Link>
    </li>
  );
}

NavbarItem.propTypes = {
  link: PropTypes.string,
  title: PropTypes.string.isRequired,
  collapsed: PropTypes.bool,
  // eslint-disable-next-line react/forbid-prop-types
  icon: PropTypes.object.isRequired,
  warn: PropTypes.bool,
  onClick: PropTypes.func,
};

NavbarItem.defaultProps = {
  warn: false,
  onClick: null,
  link: '',
  collapsed: true,
};
