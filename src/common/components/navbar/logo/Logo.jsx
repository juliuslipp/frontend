import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import classes from './Logo.module.scss';

export default function NavbarLogo(props) {
  const { collapsed, link } = props;

  return (
    <li className={classes.listItem}>
      <Link to={link} className={classes.link}>
        <span
          className={classNames({
            [classes.textNotCollapsed]: !collapsed,
          })}
        >
          P
        </span>
        <span
          className={classNames(classes.textNotCollapsed, classes.textLong, {
            [classes.textLongFadeIn]: !collapsed,
          })}
        >
          rogress
        </span>
      </Link>
    </li>
  );
}

NavbarLogo.propTypes = {
  collapsed: PropTypes.bool.isRequired,
  link: PropTypes.string,
};

NavbarLogo.defaultProps = {
  link: '/dashboard',
};
