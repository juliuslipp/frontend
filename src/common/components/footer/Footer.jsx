import React from 'react';
import { NavLink as Link } from 'react-router-dom';
import classes from './Footer.module.scss';

export default function Footer() {
  return (
    <footer className={classes.footer}>
      <div className={classes.container}>
        <div className={classes.linkContainer}>
          <Link to="/about">
            <span>Über Uns</span>
          </Link>
          <Link to="/help">
            <span> Hilfe</span>
          </Link>
          <Link to="/agb">
            <span> AGB</span>
          </Link>
          <Link to="/impressum">
            <span>Impressum</span>
          </Link>
        </div>
        <div className={classes.progressContainer}>
          <span>Progress © {` ${new Date().getFullYear()}`}</span>
        </div>
      </div>
    </footer>
  );
}
