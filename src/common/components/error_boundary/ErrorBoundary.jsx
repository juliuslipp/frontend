/* eslint-disable react/no-unused-state */
import React from 'react';
import CancelIcon from '@material-ui/icons/Cancel';
import {
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  DialogContentText,
  TextField,
  IconButton,
  Typography,
} from '@material-ui/core';

import classes from './ErrorBoundary.module.scss';

export default class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: '',
      info: '',
      open: false,
      text: '',
    };
  }

  componentDidCatch(error, info) {
    this.setState({ open: true, error, info });
    // eslint-disable-next-line no-console
    console.error({ error, info });
  }

  onTextChange(event) {
    if (event.target.value && event.target.value.length < 1000)
      this.setState({ text: event.target.value });
  }

  render() {
    const { content, children, intl, ...restProps } = this.props;
    const { open, text } = this.state;
    const childrenWithProps = React.Children.map(children, (child) =>
      React.cloneElement(child, restProps)
    );

    return (
      <>
        {childrenWithProps}
        <Dialog open={open} className={classes.dialog}>
          <div className={classes.titleContainer}>
            <Typography className={classes.titleText}>
              Leider ist ein Fehler aufgetreten !
            </Typography>
            <IconButton
              className={classes.iconButton}
              onClick={() => this.setState({ open: false })}
            >
              <CancelIcon />
            </IconButton>
          </div>
          <DialogContent>
            <DialogContentText>
              <Typography variant="body">
                Leider gab es einen Fehler. Erzähl uns was genau passiert ist:
              </Typography>
              <div className={classes.textField}>
                <TextField
                  autoFocus
                  multiline
                  onChange={(e) => this.onTextChange(e)}
                  fullWidth
                  value={text}
                  className={classes.textField}
                  variant="outlined"
                  rows={2}
                  rowsMax={8}
                />
              </div>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={() => this.setState({ open: false })}
              variant="contained"
              color="primary"
              className={classes.button}
            >
              <Typography variant="body1" className={classes.buttonText}>
                Senden
              </Typography>
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  }
}
