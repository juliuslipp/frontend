import axios from 'axios';

const API_URL = `${process.env.REACT_APP_API_URL}/api/v1`;
const TOKEN_NAME = 'progressToken';
const USER_TYPE_NAME = 'progressUserType';
const callbacks = [];

export const setLocalStorage = (token, userType) => {
  localStorage.setItem(TOKEN_NAME, token);
  localStorage.setItem(USER_TYPE_NAME, userType);
  callbacks.forEach((callback) => {
    if (callback && typeof callback === 'function') {
      callback(!!token, userType);
    }
  });
};

export const isAuthorized = (cb) => {
  callbacks.push(cb);
  cb(!!localStorage.getItem(TOKEN_NAME), localStorage.getItem(USER_TYPE_NAME));
};

export const axiosInstance = axios.create({
  baseURL: API_URL,
});

axiosInstance.interceptors.request.use((config) => {
  const token = localStorage.getItem(TOKEN_NAME);
  // eslint-disable-next-line no-param-reassign
  if (token) config.headers.Authorization = `Bearer ${token}`;
  return config;
});

axiosInstance.interceptors.response.use(undefined, (err) => {
  if (err.response && err.response.status === 401) {
    localStorage.removeItem(TOKEN_NAME);
  }
  return Promise.reject(err);
});
