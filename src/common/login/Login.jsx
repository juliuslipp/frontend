import React from 'react';
import { Link, Box, Typography } from '@material-ui/core/';
import LoginForm from '../components/login/form/Form';
import classes from './Login.module.scss';

export default function ScreensLoginForm() {
  return (
    <div>
      <h1 className={classes.loginTitle}>Progress</h1>
      <div className={classes.paper}>
        <div className={classes.info}>
          <p>
            Progress hilft dir, deinen Unterricht zu
            <span> dokumentieren </span>
            und mit anderen Lehrern in Kontakt zu bleiben.
          </p>
          <p>
            <span>Unterrichtsmaterialien</span> können einfach ausgetauscht und bewertet werden.
          </p>
          <p>
            Hilf anderen Lehrkräften dabei, ihren Unterricht zu organisieren, und lasse dich vom
            Unterricht anderer <span className={classes.keywords}>inspirieren</span>.
          </p>
          <p>
            Unser Ziel von Progress liegt darin,
            <span> Unterrichtsforschung</span> voranzutreiben und dabei mit unseren gewonnenen
            Erkenntnissen die
            <span> Aus- und Weiterbildung</span> neuer Lehrkräfte zu vereinfachen und zu verbessern.
            Sei dabei.
          </p>
        </div>
        <LoginForm />
      </div>
      <Box className={classes.box}>
        <Typography variant="body2" color="textSecondary" align="center">
          {'Copyright © '}
          <Link color="inherit" href="/">
            Progress
          </Link>
          {` ${new Date().getFullYear()}`}
        </Typography>
      </Box>
    </div>
  );
}
