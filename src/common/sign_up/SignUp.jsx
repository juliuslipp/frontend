/* eslint-disable react/jsx-wrap-multilines */
import React, { useState } from 'react';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import {
  Button,
  Avatar,
  Checkbox,
  FormControlLabel,
  FormHelperText,
  Container,
  Typography,
  Grid,
  TextField,
  CssBaseline,
} from '@material-ui/core';
import UserApiService from '../services/UserApiService';
import classes from './SignUp.module.scss';
import { GERMAN_FEDERAL_STATES, removeEmptyValuesFromObject } from '../utils/Utils';

export default function SignUp() {
  const history = useHistory();

  const [errorMessage, setErrorMessage] = useState('');

  const [userType, setUserType] = useState(false);
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [birthDate, setBirthDate] = useState('');
  const [organization, setOrganization] = useState('');
  const [address, setAddress] = useState({
    state: '',
    city: '',
    postalCode: '',
    streetAdditive: '',
    street: '',
  });

  function updateAddress(label, value) {
    setAddress((prev) => ({ ...prev, [label]: value }));
  }

  function handleSubmit(e) {
    e.preventDefault();
    const apiCallObject = removeEmptyValuesFromObject({
      firstName,
      lastName,
      email,
      password,
      address,
      organization,
      birthDate,
      userType: userType ? 'SCIENTIST' : 'TEACHER',
    });
    UserApiService.register(apiCallObject)
      .then(() => {
        history.push('/login');
      })
      .catch(() => {
        setErrorMessage('Dieser Account exsistiert bereits!');
      });
  }

  function verifyAddress() {
    return Object.values(address).reduce(
      (acc, curr) => {
        if (curr.length === 0) acc.hasEmptyValue = true;
        else acc.hasValue = true;
        if (acc.hasValue && acc.hasEmptyValue) acc.isValid = false;
        return acc;
      },
      { hasEmptyValue: false, hasValue: false, isValid: true }
    ).isValid;
  }

  function validateForm() {
    return (
      email &&
      email.length > 0 &&
      password &&
      password.length > 0 &&
      firstName &&
      firstName.length > 0 &&
      lastName &&
      lastName.length > 0 &&
      verifyAddress()
    );
  }

  return (
    <div className={classes.background}>
      <h1 className={classes.loginHeader}>Progress</h1>

      <Container component="div" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Registrierung
          </Typography>
          <form className={classes.form} noValidate onSubmit={handleSubmit}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="given-name"
                  variant="outlined"
                  required
                  fullWidth
                  label="Vorname"
                  autoFocus
                  onChange={(e) => {
                    setFirstName(e.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  label="Nachname"
                  autoComplete="family-name"
                  onChange={(e) => {
                    setLastName(e.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={12}>
                <TextField
                  fullWidth
                  variant="outlined"
                  label="Geburtstag"
                  type="date"
                  value={birthDate}
                  autoComplete="bday"
                  onChange={(e) => {
                    setBirthDate(e.target.value);
                  }}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={12}>
                <TextField
                  label="Bundesland"
                  autoComplete="address-line3"
                  select
                  value={address.state}
                  variant="outlined"
                  fullWidth
                  SelectProps={{
                    native: true,
                  }}
                  onChange={(e) => {
                    updateAddress('state', e.target.value);
                  }}
                >
                  {GERMAN_FEDERAL_STATES.map((fState) => (
                    <option key={fState} value={fState}>
                      {fState}
                    </option>
                  ))}
                </TextField>
              </Grid>
              <Grid item xs={8} sm={8}>
                <TextField
                  value={address.city}
                  variant="outlined"
                  fullWidth
                  label="Stadt"
                  onChange={(e) => {
                    updateAddress('city', e.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4}>
                <TextField
                  value={address.postalCode}
                  variant="outlined"
                  fullWidth
                  label="PLZ"
                  autoComplete="postal-code"
                  onChange={(e) => {
                    updateAddress('postalCode', e.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={8} sm={8}>
                <TextField
                  value={address.street}
                  variant="outlined"
                  fullWidth
                  label="Straße"
                  autoComplete="address-line1"
                  onChange={(e) => {
                    updateAddress('street', e.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4}>
                <TextField
                  value={address.streetAdditive}
                  variant="outlined"
                  fullWidth
                  label="Hs.-Nr."
                  autoComplete="address-line2"
                  onChange={(e) => {
                    updateAddress('streetAdditive', e.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  value={organization}
                  variant="outlined"
                  fullWidth
                  label="Organisation"
                  autoComplete="organization"
                  onChange={(e) => {
                    setOrganization(e.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  label="Email Adresse"
                  autoComplete="email"
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  label="Passwort"
                  type="password"
                  onChange={(e) => {
                    setPassword(e.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <FormControlLabel
                  control={
                    <Checkbox
                      label="Wissenschaftler"
                      checked={userType}
                      onChange={() => setUserType(!userType)}
                    />
                  }
                  label="Wissenschaftler"
                />
              </Grid>
              <Grid item xs={12}>
                <Button
                  disabled={!validateForm()}
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                >
                  Registrieren
                </Button>
                {!!errorMessage && (
                  <FormHelperText error id="component-error-text">
                    {errorMessage}
                  </FormHelperText>
                )}
              </Grid>
              <Grid item>
                <Link to="/">Sie haben schon einen Account? Loggen sie sich ein!</Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Container>
    </div>
  );
}
