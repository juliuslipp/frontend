import React from 'react';

import './Impressum.modules.scss';

export default function ScreensInformationImpressum() {
  return (
    <div>
      <h1 className="pageHeader">Impressum</h1>
      <div className="sheetWrapper">
        <h2 className="sheetHeader"> Progress GmbH </h2>
        <h2 className="sheetHeader"> Mittelweg 177 </h2>
        <h2 className="sheetHeader"> 20148 Hamburg Germany </h2>
        <h2 className="sheetHeader"> Tel: +49 40 42838-0</h2>
        <h2 className="sheetHeader"> Fax: +49 40 42838-6594</h2>
      </div>
    </div>
  );
}
