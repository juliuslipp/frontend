import React from 'react';
import { Link } from 'react-router-dom';

import './Help.modules.scss';

export default function ScreensInformationHelp() {
  return (
    <div>
      <h1 className="pageHeader">Hilfe</h1>
      <div className="sheetWrapper">
        <h2 className="sheetHeader">
          <Link to="/help"> Hilf Center</Link>
        </h2>
        <h2 className="sheetHeader">
          <a href="mailto:julius.lipp@studium.uni-hamburg.de">Kontaktieren Sie uns.</a>
        </h2>
        <h2 className="sheetHeader">
          <Link to="/help"> Datenschutzregelungen</Link>
        </h2>
      </div>
    </div>
  );
}
