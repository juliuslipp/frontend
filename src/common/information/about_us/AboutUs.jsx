import React from 'react';

import './AboutUs.modules.scss';

export default function ScreensInformationAboutUs() {
  return (
    <div>
      <h1 className="pageHeader">Über uns</h1>
      <div className="sheetWrapper">
        <h2 className="sheetHeader">Team Progress:</h2>
        <h2 className="sheetHeader">Ugur Turhan</h2>
        <h2 className="sheetHeader">Julius Lipp</h2>
        <h2 className="sheetHeader">Merve Karatas</h2>
        <h2 className="sheetHeader">Emran Sayedi</h2>
        <h2 className="sheetHeader">Edin Beganovic</h2>
        <h2 className="sheetHeader">Federico Cañadas</h2>
      </div>
    </div>
  );
}
