import React from 'react';

export default function ScreensInformationAGB() {
  return (
    <div>
      <h1 className="pageHeader">AGB</h1>
      <div className="sheetWrapper">
        <h2 className="sheetHeader">
          Allgemeine Nutzungs- und Geschäftsbedingungen. Progress Stand 05/2018. Einleitung: Die
          nachfolgenden Allgemeinen Nutzungs- und Geschäftsbedingungen regeln den rechtlichen Rahmen
          für die Beziehung zwischen der Progress GmbH (Progress) und ihren Kunden in Bezug auf die
          Nutzung der Online Schulplattform Progress sowie in Bezug auf allfällige weitere
          gegenwärtig und zukünftig von Progress angebotenen Dienstleistungen. § 1 Allgemeines (1)
          Die Allgemeinen Nutzungs- und Geschäftsbedingungen von Progress gelten ausschließlich;
          entgegenstehende oder von diesen Allgemeinen Nutzungs- und Geschäftsbedingungen
          abweichende Bedingungen des Nutzers erkennt Progress nicht an, es sei denn, Progress hätte
          ausdrücklich schriftlich ihrer Geltung zugestimmt. Die Allgemeinen Nutzungs- und
          Geschäftsbedingungen von Progress gelten auch dann, wenn Progress in Kenntnis
          entgegenstehender oder von diesen Allgemeinen Nutzungs- und Geschäftsbedingungen
          abweichender Bedingungen dem Kunden die Nutzung der Plattform vorbehaltlos gewährt. (2)
          Alle Vereinbarungen, die zwischen Progress und dem Kunden zwecks Ausführung dieses
          Vertrages getroffen werden, sind in diesem Vertrag schriftlich niedergelegt. § 2
          Vertragsgegenstand (1) Progress bietet Kunden eine Online Schulplattform zur Nutzung an
          (Software-as-a-Service-Modell). Der im Rahmen der Schulplattform dem Kunden und seinen
          Nutzern verfügbar gemachte Funktionsumfang ergibt sich aus dem Nutzungsvertrag. (2)
          InfoMentor erbringt weitere Dienstleistungen gemäß besonderer Vereinbarung, namentlich ein
          Helpdesk für Auskünfte. (3) Progress erlaubt dem Kunden, die Schulplattform im angebotenen
          Umfang gegen Bezahlung der im Nutzungsvertrag vereinbarten Gebühren zu nutzen, indem er
          dem Kunden passwortgeschützten Zugriff über das Internet auf die Schulplattform gewährt.
          (4) Die von Progress geschuldeten Leistungen ergeben sich abschließend aus dem
          Nutzungsvertrag, der auf diese Allgemeinen Nutzungs- und Geschäftsbedingungen Bezug nimmt,
          diesen Allgemeinen Nutzungs- und Geschäftsbedingungen, sowie ergänzend aus den Angaben,
          die Progress unter ihrer Webseite bekannt macht. (5) Es ist der Anspruch von Progress, die
          Schulplattform mit Blick auf ihre technischen, organisatorischen und bildungspolitischen
          Rahmenbedingungen laufend zu aktualisieren und nützlich zu erhalten. Progress hat zur
          Wahrung des Qualitätsstandards, aber auch im Hinblick auf technische oder wirtschaftliche
          Entwicklungen, das Recht, die Schulplattform und die darunter angebotenen Inhalte
          jederzeit auszuweiten, anzupassen oder einzuschränken. § 3 Nutzungsrecht (1) Dem Kunden
          wird das nicht ausschließliche, auf die Laufzeit gemäß Nutzungsvertrag befristete, auf
          Dritte nicht übertragbare Recht zur bestimmungsgemäßen Nutzung der Schulplattform im
          abonnierten Umfang (“Nutzungsrecht”) eingeräumt. (2) Das Nutzungsrecht umfasst das Recht,
          berechtigte Nutzer des Kunden mit Zugangsdaten auszustatten. Der Kunde sorgt dafür, dass
          berechtigte Nutzer die Schulplattform nur in Übereinstimmung mit dem Nutzungsvertrag sowie
          diesen Allgemeinen Nutzungs- und Geschäftsbedingungen benutzen. (3) Berechtigte „Nutzer“
          sind abschließend : Personen der Schulverwaltung des Kunden („Schulverwaltung”) Vorsteher
          der Schulverwaltung („Schulleiter”) Personen, die aktuell dem Lehrkörper angehören oder
          auf der Grundlage eines schriftlichen Auftrags für den Kunden Lehraufträge ausführen
          („Lehrer”) Lehrer mit besonderer Stellung („Klassenlehrer”) beim Kunden als aktive Schüler
          registrierte Personen („Schüler”) (4) (5) Vorbehaltlich einer anderslautenden
          schriftlichen Vereinbarung mit InfoMentor regelt § 3 der Allgemeinen Nutzungs- und
          Geschäftsbedingungen abschließend den Inhalt des Nutzungsrechts. Eine darüber
          hinausgehende Verwendung der Schulplattform ist unzulässig.
        </h2>
      </div>
    </div>
  );
}
