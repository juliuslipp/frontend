/* eslint-disable react/jsx-props-no-spreading */
import React, { useState, useEffect } from 'react';
import { CircularProgress } from '@material-ui/core';

import CalendarComponent from 'Teaching/components/calendar/Calendar';
import CalendarApiService from 'Teaching/services/CalendarApiService';

import classes from './Calendar.module.scss';

function Calendar() {
  const [isLoading, setIsLoading] = useState(true);
  const [calendar, setCalendar] = useState({});

  const fetchAppointmentData = () => {
    setIsLoading(true);
    CalendarApiService.getCalendar().then((cal) => {
      setCalendar(cal);
      setIsLoading(false);
    });
  };

  useEffect(() => {
    fetchAppointmentData();
  }, []);

  const putAppointmentData = async (data) => {
    if (data && !isLoading) {
      await CalendarApiService.setAppointments(data);
    }
  };

  return (
    <div className={classes.container}>
      {isLoading ? (
        <div className={classes.centerContainer}>
          <CircularProgress className={classes.centered} />
        </div>
      ) : (
        <CalendarComponent
          onChange={putAppointmentData}
          appointmentData={calendar.appointmentData}
          {...calendar}
        />
      )}
    </div>
  );
}

Calendar.propTypes = {};

export default Calendar;
