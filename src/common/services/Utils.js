// eslint-disable-next-line import/prefer-default-export
export function onApiCallProgress(callback) {
  return (progressEvent) => {
    callback(Math.round((progressEvent.loaded * 100) / progressEvent.total));
  };
}

export function getClient(instance, config) {
  const client = Object.assign(Object.create(Object.getPrototypeOf(instance)), instance);
  client.defaults = { ...client.defaults, ...config };
  return client;
}

export function handleHTTPErrorCodes(errorCodeOperationsObject, defaultOperation) {
  return (err) => {
    // eslint-disable-next-line react/destructuring-assignment
    if (err) {
      if (
        errorCodeOperationsObject &&
        err.response &&
        errorCodeOperationsObject[err.response.status] &&
        typeof errorCodeOperationsObject[err.response.status] === 'function'
      )
        return errorCodeOperationsObject[err.response.status]();
      if (typeof defaultOperation === 'function') return defaultOperation();
    }
    return null;
  };
}

export function downloadObjectAsJson(exportObj, exportName) {
  const dataStr = `data:text/json;charset=utf-8,${encodeURIComponent(
    JSON.stringify(exportObj, null, '\t')
  )}`;
  const node = document.createElement('a');
  node.setAttribute('href', dataStr);
  node.setAttribute('download', `${exportName}.json`);
  document.body.appendChild(node);
  node.click();
  node.remove();
}
