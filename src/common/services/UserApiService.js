import { axiosInstance as client, setLocalStorage } from '../middleware/Middleware';

const endpoint = `/user`;

function UserApiService() {
  return {
    async login(email, password) {
      return client
        .get('/login', { params: { email, password } })
        .then((response) => response.data)
        .then((data) => {
          const arr = `${data}`.split(' ');
          setLocalStorage(arr[2], arr[1]);
          return arr[0];
        });
    },
    async logOut() {
      return client.put('/logout').finally(() => {
        setLocalStorage('', '');
      });
    },
    async register(user) {
      return client.post(endpoint, user).then((response) => response.data);
    },

    async changePassword(oldPassword, newPassword) {
      return client.put(`${endpoint}/password`, { params: { oldPassword, newPassword } });
    },

    async getUserInfo() {
      return client.get(`${endpoint}/info`).then((response) => response.data);
    },

    async setUserInfo(userInfo) {
      return client.put(`${endpoint}/info`, { ...userInfo });
    },
    async getMockUserInfo() {
      return client.get(`${endpoint}/info/mock`).then((res) => res.data);
    },
  };
}

export default UserApiService();
