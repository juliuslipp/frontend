import { axiosInstance } from 'Common/middleware/Middleware';
import { getClient } from 'Common/services/Utils';

const endpoint = '/document';

function DocumentApiService(config = {}) {
  const client = Object.keys(config).length > 0 ? getClient(axiosInstance, config) : axiosInstance;

  return {
    async createDocument(document) {
      return client.post(endpoint, document).then((res) => res.data);
    },
    async editDocument(documentId, document) {
      return client.put(`${endpoint}/${documentId}`, document).then((res) => res.data);
    },
    async getDocument(documentId) {
      return client.get(`${endpoint}/${documentId}`, config).then((res) => res.data);
    },
    async getMockDocument() {
      return client.get(`${endpoint}/mock`).then((res) => res.data);
    },
    async getMockDocumentList() {
      return client.get(`${endpoint}/list/mock`).then((res) => res.data);
    },
    async getDocumentList() {
      return client.get(`${endpoint}/list`).then((res) => res.data);
    },
    async deleteDocument(documentId) {
      return client.delete(`${endpoint}/${documentId}`);
    },
    async getDocumentListByUser() {
      return client.get(`${endpoint}/list/editable`).then((res) => res.data);
    },
    async getDocumentMetaDataList() {
      return client.get(`${endpoint}/list/meta`).then((res) => res.data);
    },
  };
}

export const DocumentApiServiceConfigurable = DocumentApiService;

export default DocumentApiService();
