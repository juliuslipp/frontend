import React from 'react';
import { Link } from 'react-router-dom';

export default function ScreensNotFound() {
  return (
    <div>
      <div>
        <h1>404 - Not Found!</h1>
        <Link to="/"> Hier geht es zurück! </Link>
      </div>
    </div>
  );
}
