export const GERMAN_FEDERAL_STATES = [
  '',
  'Baden-Württemberg',
  'Bayern',
  'Berlin',
  'Brandenburg',
  'Bremen',
  'Hessen',
  'Hamburg',
  'Mecklenburg-Vorpommern',
  'Niedersachsen',
  'Nordrhein-Westfalen',
  'Rheinland-Pfalz',
  'Saarland',
  'Sachsen',
  'Sachsen-Anhalt',
  'Schleswig-Holstein',
  'Thüringen',
];

/**
 *  Removes empty values from js object.
 *
 * @param obj to remove empty values from
 * @returns {object} the same object without empty values
 */
export function removeEmptyValuesFromObject(obj) {
  if (typeof obj === 'object') {
    const copiedObject = { ...obj };
    Object.keys(obj).forEach((key) => {
      if (typeof obj[key] === 'object') copiedObject[key] = removeEmptyValuesFromObject(obj[key]);
      else if (
        obj[key] === null ||
        obj[key] === undefined ||
        (typeof obj[key] === 'string' && obj[key].length === 0)
      )
        delete copiedObject[key];
    });
    return copiedObject;
  }
  return obj;
}

/**
 *  Extracts base64 Data from JS File object
 *
 * @param file JS File Object
 * @returns {Promise} with base64 encoded data
 */
export async function toBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}
