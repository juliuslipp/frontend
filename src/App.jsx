import React, { useEffect, useState } from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';

import LoginPage from 'Common/login/Login';
import SignUpPage from 'Common/sign_up/SignUp';
import AboutUsPage from 'Common/information/about_us/AboutUs';
import AGBPage from 'Common/information/agb/AGB';
import HelpPage from 'Common/information/help/Help';
import ImpressiumPage from 'Common/information/impressum/Impressum';
import NotFoundPage from 'Common/not_found/NotFound';

import { isAuthorized } from 'Common/middleware/Middleware';
import TeachingApp from 'Teaching/TeachingApp';
import ResearchApp from 'Research/ResarchApp';
import PrivateRoute from 'Common/components/PrivateRoute';
import ErrorBoundary from 'Common/components/error_boundary/ErrorBoundary';
import ScrollToTop from 'Common/utils/ScrollToTop';

const RESEARCH_PREFIX = 'research';

export default function ScreensRoot() {
  const [auth, setAuth] = useState({ isAuth: false, isTeacher: false });

  const redirection = () => {
    if (!auth.isAuth) return '/login';
    return auth.isTeacher ? '/dashboard' : `/${RESEARCH_PREFIX}/dashboard`;
  };

  const handleTeacherUpdate = (authObj) => {
    setAuth((prev) => ({ ...prev, ...authObj }));
  };

  useEffect(() => {
    isAuthorized((isAuth, userType) => {
      if (!!userType && userType.length > 0)
        handleTeacherUpdate({ isAuth, isTeacher: userType === 'TEACHER' });
      else handleTeacherUpdate({ isAuth });
    });
  }, []);

  const InfoPages = () => (
    <>
      <Route path="/about" exact component={AboutUsPage} />
      <Route path="/agb" exact component={AGBPage} />
      <Route path="/help" exact component={HelpPage} />
      <Route path="/impressum" exact component={ImpressiumPage} />
    </>
  );

  const TargetApp = () =>
    auth.isTeacher ? (
      <TeachingApp auth={auth.isAuth}>
        <InfoPages />
      </TeachingApp>
    ) : (
      <ResearchApp auth={auth.isAuth} prefix={RESEARCH_PREFIX}>
        <InfoPages />
      </ResearchApp>
    );

  return (
    <main>
      <ErrorBoundary>
        <BrowserRouter>
          <ScrollToTop />
          <Switch>
            <Route path="/" exact render={() => <Redirect to={redirection()} />} />
            <PrivateRoute
              path="/login"
              exact
              component={LoginPage}
              isAuthorized={!auth.isAuth}
              redirectionPath="/"
            />
            <PrivateRoute
              path="/signup"
              exact
              component={SignUpPage}
              isAuthorized={!auth.isAuth}
              redirectionPath="/"
            />
            <TargetApp />
            <Route component={NotFoundPage} />
          </Switch>
        </BrowserRouter>
      </ErrorBoundary>
    </main>
  );
}
