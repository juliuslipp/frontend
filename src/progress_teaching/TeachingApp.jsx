/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { AddRounded, HomeRounded, PersonRounded, EventRounded } from '@material-ui/icons';

import Navbar from 'Common/components/navbar/Navbar';
import Footer from 'Common/components/footer/Footer';
import { WrappedPrivateRoute } from 'Common/components/PrivateRoute';
import NavbarItem from 'Common/components/navbar/item/Item';
import CalendarPage from '../common/calendar/Calendar';
import DashboardPage from './dashboard/Dashboard';
import UserProfilePage from './user_profile/UserProfile';
import CreateDocumentPage from './create_document/CreateDocument';
import classes from './TeachingApp.module.scss';

export default function TeachingApp({ children, auth }) {
  const wrapper = (properties) => <div className={classes.appContainer}>{properties.children}</div>;
  return (
    <>
      <Navbar>
        <NavbarItem title="Übersicht" icon={HomeRounded} link="/dashboard" />
        <NavbarItem title="Neues Dokument" icon={AddRounded} link="/document/create" />
        <NavbarItem title="Profil" icon={PersonRounded} link="/user/profile" />
        <NavbarItem title="Kalendar" icon={EventRounded} link="/user/calendar" />
      </Navbar>
      <WrappedPrivateRoute
        wrapper={wrapper}
        path="/dashboard"
        exact
        component={DashboardPage}
        isAuthorized={auth}
      />
      <WrappedPrivateRoute
        wrapper={wrapper}
        path="/document/create"
        exact
        component={CreateDocumentPage}
        isAuthorized={auth}
      />
      <WrappedPrivateRoute
        wrapper={wrapper}
        path="/user/profile"
        component={UserProfilePage}
        isAuthorized={auth}
      />
      <WrappedPrivateRoute
        wrapper={wrapper}
        path="/user/calendar"
        component={CalendarPage}
        isAuthorized={auth}
      />
      {children}
      <Footer />
    </>
  );
}
