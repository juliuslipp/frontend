import React, { useState, useEffect } from 'react';
import dateFormat from 'dateformat';
import classes from './DateTime.module.scss';

const WEEK_DAYS = [
  'Sun',
  'Mon',
  'Tue',
  'Wed',
  'Thu',
  'Fri',
  'Sat',
  'Sonntag',
  'Montag',
  'Dienstag',
  'Mittwoch',
  'Donnerstag',
  'Freitag',
  'Samstag',
];

const MONTHS = [
  'Jan',
  'Feb',
  'Mär',
  'Apr',
  'Mai',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Okt',
  'Nov',
  'Dez',
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

dateFormat.i18n = {
  dayNames: WEEK_DAYS,
  monthNames: MONTHS,
  timeNames: ['a', 'p', 'am', 'pm', 'A', 'P', 'AM', 'PM'],
};

export default function DateTime() {
  const [date, setDate] = useState(new Date());
  const [mounted, setMounted] = useState(false);
  useEffect(() => {
    setMounted(true);
    const updateTime = () => {
      if (mounted) setDate(new Date());
    };
    setInterval(updateTime, 15000);
    return () => {
      setMounted(false);
      clearInterval(updateTime);
    };
  }, []);

  return (
    <div className={classes.container}>
      <p>
        <span>{dateFormat(date, 'dddd, d. mmm. yyyy')}</span>
      </p>
      <p>
        <span>{`${dateFormat(date, 'HH:MM')} Uhr`}</span>
      </p>
    </div>
  );
}
