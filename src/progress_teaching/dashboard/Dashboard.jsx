import React, { useState, useEffect } from 'react';

import UserApiService from 'Common/services/UserApiService';
import illustrations from './components/illustration_images';
import DashboardDateTime from './components/date_time/DateTime';
import classes from './Dashboard.module.scss';

const getImageUrl = () => `url(${illustrations[Math.round(Math.random() * illustrations.length)]})`;

export default function ScreensUserDashboard() {
  const [name, setName] = useState('');
  const [imageUrl, setImageUrl] = useState('');

  useEffect(() => {
    UserApiService.getUserInfo().then((userData) => {
      setName(userData.firstName);
    });
    setImageUrl(getImageUrl());
  }, []);

  return (
    <div
      className={classes.container}
      style={{
        backgroundImage: imageUrl,
      }}
    >
      <div className={classes.header}>
        <h1 className="pageHeader">{name ? `Hi, ${name}.` : 'Hi.'}</h1>
        <DashboardDateTime />
      </div>
    </div>
  );
}
