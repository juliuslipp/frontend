import React, { useEffect, useState } from 'react';
import { CakeRounded, HomeRounded, MailOutlineRounded } from '@material-ui/icons';
import { CircularProgress, Grid, InputAdornment, TextField } from '@material-ui/core';
import dateFormat from 'dateformat';
import classNames from 'classnames';

import UserApiService from 'Common/services/UserApiService';
import DocumentApiService from 'Common/services/DocumentApiService';
import classes from './UserProfile.module.scss';
import DocumentViewComponent from './components/document/ViewComponent';

export default function ScreensUserInfo() {
  const [userInfo, setUserInfo] = useState({
    firstName: null,
    lastName: null,
    email: null,
    birthDate: null,
    address: null,
  });

  const [isLoading, setIsLoading] = useState(true);

  const [documentMetaDataList, setDocumentMetaDataList] = useState([]);

  const fetchUserData = () => {
    setIsLoading(true);
    UserApiService.getUserInfo()
      .then((userInformation) => {
        setUserInfo(userInformation);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const fetchDocumentMetaData = () => {
    DocumentApiService.getDocumentMetaDataList().then((metaDataList) =>
      setDocumentMetaDataList(metaDataList)
    );
  };

  useEffect(() => {
    fetchUserData();
    fetchDocumentMetaData();
  }, []);

  return (
    <div>
      <div className={classes.sheetWrapper}>
        {isLoading ? (
          <CircularProgress />
        ) : (
          <>
            <h1 className={classNames(classes.sheetHeader, classes.header)}>
              {`${userInfo.firstName} ${userInfo.lastName}`}
            </h1>
            <div className={classes.initials}>
              <Grid className={classes.profileData} container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    value={userInfo.email}
                    disabled
                    variant="outlined"
                    label="Email"
                    fullWidth
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <MailOutlineRounded />
                        </InputAdornment>
                      ),
                    }}
                  />
                </Grid>
                {userInfo.birthDate && (
                  <Grid item xs={12}>
                    <TextField
                      value={dateFormat(Date.parse(userInfo.birthDate), 'dd.mm.yyyy')}
                      disabled
                      variant="outlined"
                      label="Geburtstag"
                      fullWidth
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <CakeRounded />
                          </InputAdornment>
                        ),
                      }}
                    />
                  </Grid>
                )}
                <Grid item xs={12}>
                  <TextField
                    value={
                      userInfo.address &&
                      `${userInfo.address.street} ${userInfo.address.streetAdditive}, ${userInfo.address.postalCode} ${userInfo.address.city}`
                    }
                    fullWidth
                    disabled
                    variant="outlined"
                    label="Adresse"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <HomeRounded />
                        </InputAdornment>
                      ),
                    }}
                  />
                </Grid>
              </Grid>
              <img
                src="https://cdn4.iconfinder.com/data/icons/instagram-ui-twotone/48/Paul-18-512.png"
                alt="ProfilePicture"
                className={classes.profileImage}
              />
            </div>
          </>
        )}
      </div>
      <div className={classes.sheetWrapper}>
        <h2 className={classes.sheetHeader}>Dokumentation</h2>
        <div className={classes.documentViewContainer}>
          {documentMetaDataList.map((docMetaData, i) => (
            // eslint-disable-next-line react/no-array-index-key
            <DocumentViewComponent documentMetaData={docMetaData} key={i} />
          ))}
        </div>
      </div>
    </div>
  );
}
