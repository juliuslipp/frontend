/* eslint-disable react/jsx-props-no-spreading */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { v4 as uuid } from 'uuid';
import { Button } from '@material-ui/core';

import ProgressDialog from 'Teaching/components/dialog/ProgressDialog';
import PhaseForm from 'Teaching/components/document/phase/form/Form';
import GeneralForm from 'Teaching/components/document/general/form/Form';
import CommentForm from 'Teaching/components/document/comment/form/From';
import MaterialForm from 'Teaching/components/document/material/form/Form';
import { DocumentApiServiceConfigurable } from 'Common/services/DocumentApiService';
import { onApiCallProgress } from 'Common/services/Utils';
import AlertDialog from 'Common/components/dialog/AlertDialog';
import { useHistory } from 'react-router';
import classes from './ViewDialog.module.scss';

const FormElement = ({ formComponent, formDataArray, formData, ...rest }) => {
  const FormComponent = formComponent;
  const hasContent = (formDataArray && formDataArray.length > 0) || formData;

  return hasContent ? (
    <div className={classes.formContainer}>
      <h2>{rest.title}</h2>
      {formData && (
        <div className={classes.form}>
          <FormComponent data={formData} disabled {...rest} />
        </div>
      )}
      {formDataArray &&
        formDataArray.map((data) => (
          <div key={data.uniqueId} className={classes.form}>
            <FormComponent data={data} disabled {...rest} />
          </div>
        ))}
    </div>
  ) : (
    <div style={{ display: 'none' }} />
  );
};

function ViewDialog(props) {
  const { open, onClose, documentId, title } = props;
  const [document, setDocument] = useState(null);
  const [loadingProgress, setLoadingProgress] = useState(undefined);
  const [isLoading, setIsLoading] = useState(true);
  const [alertDialogOpen, setAlertDialogOpen] = useState(false);

  const history = useHistory();

  const documentApiService = DocumentApiServiceConfigurable({
    onDownloadProgress: onApiCallProgress((p) => {
      setLoadingProgress(p);
    }),
  });

  const dialogContent = (
    <div className={classes.formCardContainer}>
      <FormElement formComponent={GeneralForm} formData={document} title="Allgemeine Daten" />
      <FormElement
        formDataArray={document && document.teachingPhaseList}
        formComponent={PhaseForm}
        viewMode
        title="Unterrichtsphasen"
      />
      <FormElement
        formDataArray={document && document.materialList}
        formComponent={MaterialForm}
        viewMode
        title="Materialien"
      />
      <FormElement
        formDataArray={document && document.commentList}
        formComponent={CommentForm}
        title="Bemerkungen"
      />
    </div>
  );

  const dialogActions = (
    <Button
      variant="text"
      color="secondary"
      className={classes.dialogActions}
      onClick={() => setAlertDialogOpen(true)}
    >
      Dokument löschen
    </Button>
  );

  const addIdToList = (list) => list && list.map((obj) => Object.assign(obj, { uniqueId: uuid() }));

  const fetchDocumentData = () => {
    documentApiService.getDocument(documentId).then((documentData) => {
      const newDocument = { ...documentData };
      newDocument.teachingPhaseList = addIdToList(documentData.teachingPhaseList);
      newDocument.commentList = addIdToList(documentData.commentList);
      newDocument.materialList = addIdToList(documentData.materialList);
      setDocument(newDocument);
      setIsLoading(false);
    });
  };

  useEffect(() => {
    if (!document && open) {
      fetchDocumentData();
    }
  }, [open]);

  useEffect(() => {
    if (!isLoading) setLoadingProgress(undefined);
  }, [isLoading]);

  const handleAlertDialogClose = (accepted) => {
    if (accepted) {
      documentApiService.deleteDocument(documentId).then(() => history.push('/user/profile'));
    } else {
      setAlertDialogOpen(false);
    }
  };

  return (
    <>
      <ProgressDialog
        open={open}
        onClose={onClose}
        content={dialogContent}
        actions={dialogActions}
        title={title || documentId}
        closeIcon
        loadingProgress={loadingProgress}
        loading={isLoading}
      />
      <AlertDialog
        acceptText="Löschen"
        declineText="Abbrechen"
        contentText="Wollen Sie das Dokument wirklich löschen?"
        title="Dokument löschen"
        open={alertDialogOpen}
        handleClose={handleAlertDialogClose}
        danger
      />
    </>
  );
}

export default ViewDialog;

ViewDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  documentId: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
};
