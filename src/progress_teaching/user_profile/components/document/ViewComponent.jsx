import React, { useState } from 'react';
import PropTypes from 'prop-types';
import dateFormat from 'dateformat';

import { Button } from '@material-ui/core';
import ViewDialog from './view_dialog/ViewDialog';

export default function ViewComponent(props) {
  const { documentMetaData } = props;
  const [isOpen, setIsOpen] = useState(false);

  const { date, subject, schoolClass, topic, id } = documentMetaData;
  const documentTitle = `${topic} - ${subject},${schoolClass} (${dateFormat(date, 'dd.mm.yyyy')})`;

  return (
    <>
      <Button
        variant="outlined"
        fullWidth
        onClick={() => {
          setIsOpen((prev) => !prev);
        }}
      >
        {documentTitle}
      </Button>
      <ViewDialog
        title={documentTitle}
        documentId={id}
        open={isOpen}
        onClose={() => setIsOpen(false)}
      />
    </>
  );
}

ViewComponent.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  documentMetaData: PropTypes.object.isRequired,
};
