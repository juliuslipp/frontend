import React from 'react';
import {
  Box,
  CircularProgress,
  Dialog,
  IconButton,
  Typography,
  DialogContent,
  DialogActions,
  DialogTitle,
  Slide,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import PropTypes from 'prop-types';
import classes from './ProgressDialog.module.scss';

const Transition = React.forwardRef((props, ref) => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <Slide direction="down" ref={ref} {...props} />
));

const CircularLoadingWithLabel = ({ value }) => (
  <Box position="relative" display="inline-flex" className={classes.loadingIcon}>
    {/* eslint-disable-next-line react/jsx-props-no-spreading */}
    <CircularProgress variant="determinate" value={value} />
    <Box
      top={0}
      left={0}
      bottom={0}
      right={0}
      position="absolute"
      display="flex"
      alignItems="center"
      justifyContent="center"
    >
      <Typography variant="caption" component="div" color="textSecondary">
        {`${Math.round(value)}%`}
      </Typography>
    </Box>
  </Box>
);

export default function ProgressDialog(props) {
  const {
    onClose,
    closeIcon,
    content,
    title,
    open,
    actions,
    titleElement,
    loading,
    loadingProgress,
    maxWidth,
    fullScreen,
  } = props;

  const isLoading = loading || loadingProgress !== undefined;

  return (
    <Dialog
      TransitionComponent={Transition}
      onClose={onClose}
      open={open}
      maxWidth={maxWidth}
      fullWidth
      fullScreen={fullScreen}
    >
      {!titleElement ? (
        <DialogTitle disableTypography className={classes.dialogTitle}>
          <Typography variant="h6"> {title}</Typography>
          {closeIcon && (
            <IconButton className={classes.closeIcon} aria-label="close" onClick={onClose}>
              <CloseIcon />
            </IconButton>
          )}
        </DialogTitle>
      ) : (
        titleElement
      )}
      {loading && !loadingProgress && (
        <DialogContent dividers className={classes.loadingContainer}>
          <CircularProgress className={classes.loadingIcon} />
        </DialogContent>
      )}
      {loadingProgress !== undefined && (
        <DialogContent dividers className={classes.loadingContainer}>
          <CircularLoadingWithLabel value={loadingProgress} />
        </DialogContent>
      )}
      {!isLoading && content && <DialogContent dividers>{content}</DialogContent>}
      {actions && <DialogActions>{actions}</DialogActions>}
    </Dialog>
  );
}
ProgressDialog.propTypes = {
  title: PropTypes.string,
  closeIcon: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  content: PropTypes.element,
  actions: PropTypes.element,
  titleElement: PropTypes.element,
  loading: PropTypes.bool,
  maxWidth: PropTypes.string,
  loadingProgress: PropTypes.number,
  fullScreen: PropTypes.bool,
};

ProgressDialog.defaultProps = {
  title: undefined,
  closeIcon: false,
  content: null,
  actions: null,
  titleElement: null,
  loading: false,
  maxWidth: 'md',
  loadingProgress: undefined,
  fullScreen: false,
};
