import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { FormControl, Grid, InputLabel, MenuItem, Select, TextField } from '@material-ui/core';

import DocumentMaterialForm from 'Teaching/components/document/material/form/Form';

const TEACHING_PHASE_ENUM = Object.freeze({
  Einführung: 0,
  Hinführung: 1,
  Erarbeitung: 2,
  Sicherung: 3,
});

const durationArray = [
  '',
  '5 min',
  '10 min',
  '15 min',
  '20 min',
  '30 min',
  '45 min',
  '1:00 h',
  '1:15 h',
  '1:30 h',
  '1:45 h',
  '2:00 h',
];

export default function DocumentPhaseForm(props) {
  const { onChange, disabled, data, viewMode } = props;

  const [formData, setFormData] = useState(
    data || {
      learningPhase: '',
      duration: '',
      description: '',
      tasks: '',
      positiveNotes: '',
      problems: '',
      material: {},
    }
  );

  useEffect(() => {
    if (data) {
      setFormData(data);
    }
  }, [data]);

  useEffect(() => {
    if (onChange) onChange(formData);
  }, [formData]);

  function updateFormData(label, newData) {
    setFormData((prev) => ({ ...prev, [label]: newData }));
  }

  const hasMaterial = Object.keys(formData.material).length > 0;

  return (
    <Grid container spacing={2}>
      <Grid item xs={8}>
        <FormControl variant="outlined" disabled={disabled} fullWidth>
          <InputLabel id="phase-label">Phase</InputLabel>
          <Select
            labelId="phase-label"
            labelWidth={45}
            value={formData.learningPhase}
            defaultValue=""
            onChange={(e) => {
              updateFormData('learningPhase', e.target.value);
            }}
          >
            {Object.keys(TEACHING_PHASE_ENUM).map((teachingPhase, index) => (
              <MenuItem key={teachingPhase} value={index}>
                {teachingPhase}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={4}>
        <FormControl variant="outlined" fullWidth disabled={disabled}>
          <InputLabel id="duration-label">Dauer</InputLabel>
          <Select
            labelId="duration-label"
            labelWidth={45}
            value={formData.duration}
            defaultValue=""
            onChange={(e) => {
              updateFormData('duration', e.target.value);
            }}
          >
            {durationArray.map((phaseDuration) => (
              <MenuItem key={phaseDuration} value={phaseDuration}>
                {phaseDuration}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <TextField
          label="Durchführung"
          value={formData.description}
          helperText="Beschreibung zur Durchführung der Unterrichtsphase"
          fullWidth
          disabled={disabled}
          multiline
          variant="outlined"
          onChange={(e) => {
            updateFormData('description', e.target.value);
          }}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          label="Aufgaben"
          value={formData.tasks}
          helperText="Aufgaben an die Schüler"
          fullWidth
          multiline
          disabled={disabled}
          variant="outlined"
          onChange={(e) => {
            updateFormData('tasks', e.target.value);
          }}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          label="Positives"
          value={formData.positiveNotes}
          helperText="Was hat gut funktioniert im Unterricht?"
          fullWidth
          multiline
          disabled={disabled}
          variant="outlined"
          onChange={(e) => {
            updateFormData('positiveNotes', e.target.value);
          }}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          label="Probleme"
          value={formData.problems}
          helperText="Welche Probleme gab es?"
          fullWidth
          multiline
          disabled={disabled}
          variant="outlined"
          onChange={(e) => {
            updateFormData('problems', e.target.value);
          }}
        />
      </Grid>
      {(!viewMode || hasMaterial) && (
        <Grid item xs={12}>
          <DocumentMaterialForm
            onChange={(material) => updateFormData('material', material)}
            viewMode={viewMode}
            disabled={disabled}
            data={formData.material}
          />
        </Grid>
      )}
    </Grid>
  );
}

DocumentPhaseForm.propTypes = {
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  viewMode: PropTypes.bool,
};

DocumentPhaseForm.defaultProps = {
  disabled: false,
  onChange: undefined,
  viewMode: false,
};
