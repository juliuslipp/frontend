import React, { useState, useLayoutEffect } from 'react';
import { Grid, TextField } from '@material-ui/core';
import PropTypes from 'prop-types';

import classes from './Form.module.scss';

export default function DocumentCommentForm(props) {
  const { onChange, disabled, data } = props;

  const [formData, setFormData] = useState(
    data || {
      title: '',
      comment: '',
    }
  );

  useLayoutEffect(() => {
    if (onChange) onChange(formData);
  }, [formData]);

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <TextField
          label="Titel"
          value={formData.title}
          disabled={disabled}
          variant="outlined"
          fullWidth
          onChange={(e) => setFormData((prevState) => ({ ...prevState, title: e.target.value }))}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          label="Bemerkung"
          value={formData.comment}
          variant="outlined"
          fullWidth
          multiline
          disabled={disabled}
          className={classes.textField}
          onChange={(e) => setFormData((prevState) => ({ ...prevState, comment: e.target.value }))}
        />
      </Grid>
    </Grid>
  );
}

DocumentCommentForm.defaultProps = {
  disabled: false,
  onChange: undefined,
};

DocumentCommentForm.propTypes = {
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
};
