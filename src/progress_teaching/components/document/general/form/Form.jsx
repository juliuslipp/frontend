import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import dateFormat from 'dateformat';
import { Grid, TextField } from '@material-ui/core';

import { GERMAN_FEDERAL_STATES } from 'Common/utils/Utils';

export default function DocumentGeneralForm(props) {
  const { onChange, disabled, data } = props;

  const [formData, setFormData] = useState(
    data || {
      subject: '',
      schoolClass: '',
      startTime: dateFormat(new Date(), 'HH:MM'),
      endTime: dateFormat(new Date(), 'HH:MM'),
      date: dateFormat(new Date(), 'yyyy-mm-dd'),
      school: '',
      topic: '',
      state: '',
      textbook: '',
    }
  );

  useEffect(() => {
    if (data) setFormData(data);
  }, [data]);

  // sends the data to the parent component, when some value changes
  useEffect(() => {
    if (onChange) {
      const splitStartTime = formData.startTime.split(':');
      const splitEndTime = formData.endTime.split(':');
      onChange({
        ...formData,
        startTime: new Date(null, null, null, splitStartTime[0], splitStartTime[1]),
        endTime: new Date(null, null, null, splitEndTime[0], splitEndTime[1]),
      });
    }
  }, [formData]);

  // updates formData, when some TextField value changes
  function updateInputData(label, value) {
    setFormData((prev) => ({ ...prev, [label]: value }));
  }

  return (
    <Grid container spacing={2}>
      <Grid item xs={8}>
        <TextField
          label="Fach"
          value={formData.subject}
          variant="outlined"
          fullWidth
          required={!disabled}
          disabled={disabled}
          onChange={(e) => {
            updateInputData('subject', e.target.value);
          }}
        />
      </Grid>
      <Grid item xs={4}>
        <TextField
          label="Klasse"
          value={formData.schoolClass}
          variant="outlined"
          fullWidth
          required={!disabled}
          disabled={disabled}
          onChange={(e) => {
            updateInputData('schoolClass', e.target.value);
          }}
        />
      </Grid>

      <Grid item xs={8}>
        <TextField
          label="Schule"
          value={formData.school}
          variant="outlined"
          fullWidth
          disabled={disabled}
          onChange={(e) => {
            updateInputData('school', e.target.value);
          }}
        />
      </Grid>
      <Grid item xs={4}>
        <TextField
          label="Datum"
          type="date"
          value={formData.date}
          variant="outlined"
          fullWidth
          disabled={disabled}
          onChange={(e) => {
            updateInputData('date', e.target.value);
          }}
        />
      </Grid>

      <Grid item xs={6}>
        <TextField
          label="von"
          type="time"
          value={formData.startTime}
          variant="outlined"
          fullWidth
          disabled={disabled}
          onChange={(e) => {
            updateInputData('startTime', e.target.value);
          }}
        />
      </Grid>
      <Grid item xs={6}>
        <TextField
          label="bis"
          type="time"
          value={formData.endTime}
          variant="outlined"
          fullWidth
          disabled={disabled}
          onChange={(e) => {
            updateInputData('endTime', e.target.value);
          }}
        />
      </Grid>

      <Grid item xs={12}>
        <TextField
          label="Bundesland"
          select={!disabled}
          value={formData.state}
          variant="outlined"
          fullWidth
          required={!disabled}
          disabled={disabled}
          SelectProps={{
            native: true,
          }}
          onChange={(e) => {
            updateInputData('state', e.target.value);
          }}
        >
          {GERMAN_FEDERAL_STATES.map((fState) => (
            <option key={fState} value={fState}>
              {fState}
            </option>
          ))}
        </TextField>
      </Grid>

      <Grid item xs={12}>
        <TextField
          label="Lehrwerk"
          value={formData.textbook}
          variant="outlined"
          fullWidth
          required={!disabled}
          disabled={disabled}
          onChange={(e) => {
            updateInputData('textbook', e.target.value);
          }}
        />
      </Grid>

      <Grid item xs={12}>
        <TextField
          label="Thema"
          value={formData.topic}
          variant="outlined"
          multiline
          fullWidth
          required={!disabled}
          disabled={disabled}
          onChange={(e) => {
            updateInputData('topic', e.target.value);
          }}
        />
      </Grid>
    </Grid>
  );
}

DocumentGeneralForm.propTypes = {
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
};

DocumentGeneralForm.defaultProps = {
  disabled: false,
  onChange: undefined,
};
