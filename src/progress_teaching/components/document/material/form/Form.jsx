import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Grid, TextField } from '@material-ui/core';

import FilePreview from 'Teaching/components/file/preview/Preview';
import FileUpload from 'Teaching/components/file/upload/Upload';

export default function DocumentMaterialForm(props) {
  const { data, onChange, disabled, viewMode } = props;

  const [formData, setFormData] = useState(
    data || {
      data: [],
      description: '',
    }
  );

  useEffect(() => {
    if (data) setFormData(data);
  }, [data]);

  useEffect(() => {
    if (onChange) {
      onChange(formData);
    }
  }, [formData]);

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        {viewMode ? (
          <FilePreview files={formData.data} />
        ) : (
          <FileUpload
            onChange={(files) => {
              setFormData((prev) => ({ ...prev, data: files }));
            }}
          />
        )}
      </Grid>
      <Grid item xs={12}>
        <TextField
          label="Beschreibung"
          value={formData.description}
          helperText="Beschreibung der hinzugefügten Materialien"
          multiline
          variant="outlined"
          fullWidth
          disabled={disabled}
          onChange={(e) => {
            setFormData((prev) => ({ ...prev, description: e.target.value }));
          }}
        />
      </Grid>
    </Grid>
  );
}

DocumentMaterialForm.propTypes = {
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  viewMode: PropTypes.bool,
};

DocumentMaterialForm.defaultProps = {
  disabled: false,
  viewMode: false,
  onChange: undefined,
};
