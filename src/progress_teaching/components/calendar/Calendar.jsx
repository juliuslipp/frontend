/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect, useState } from 'react';
import {
  Appointments,
  AppointmentTooltip,
  DayView,
  MonthView,
  Scheduler,
  WeekView,
  AllDayPanel,
  Toolbar,
  ViewSwitcher,
  EditRecurrenceMenu,
  DragDropProvider,
  DateNavigator,
  AppointmentForm,
  CurrentTimeIndicator,
} from '@devexpress/dx-react-scheduler-material-ui';
import Paper from '@material-ui/core/Paper';
import { EditingState, ViewState } from '@devexpress/dx-react-scheduler';
import PropTypes from 'prop-types';

const Calendar = (props) => {
  const {
    defaultType,
    editable,
    type,
    navigatable,
    showCurrentTime,
    height,
    cellDuration,
    endDayHour,
    startDayHour,
    appointmentData,
    onChange,
  } = props;

  const [data, setData] = useState(appointmentData ? [...appointmentData] : []);

  const commitCalendarChanges = ({ added, changed, deleted }) => {
    setData((prevState) => {
      let newData = [...prevState];
      if (added) {
        const startingAddedId = newData.length > 0 ? newData[newData.length - 1].id + 1 : 0;
        newData = [...newData, { id: startingAddedId, ...added }];
      }
      if (changed) {
        newData = newData.map((appointment) =>
          changed[appointment.id] ? { ...appointment, ...changed[appointment.id] } : appointment
        );
      }
      if (deleted !== undefined) {
        newData = newData.filter((appointment) => appointment.id !== deleted);
      }
      return newData;
    });
  };

  useEffect(() => {
    if (onChange) onChange(data);
  }, [data]);

  useEffect(() => {
    setData(appointmentData);
  }, [appointmentData]);

  return (
    <Paper>
      <Scheduler data={data} height={height}>
        <ViewState defaultCurrentViewName={defaultType} currentViewName={type} />
        <EditingState onCommitChanges={commitCalendarChanges} />

        <DayView startDayHour={startDayHour} endDayHour={endDayHour} cellDuration={cellDuration} />
        <WeekView startDayHour={startDayHour} endDayHour={endDayHour} cellDuration={cellDuration} />
        <MonthView />

        <AllDayPanel />
        <Appointments />

        {(!!type || navigatable) && <Toolbar />}
        {!type && <ViewSwitcher />}
        {navigatable && <DateNavigator />}
        <EditRecurrenceMenu />

        {editable && <DragDropProvider />}

        <AppointmentTooltip showCloseButton showOpenButton={editable} />
        <AppointmentForm readOnly={!editable} />

        {showCurrentTime && <CurrentTimeIndicator />}
      </Scheduler>
    </Paper>
  );
};

const TypePropType = PropTypes.oneOf(['Day', 'Week', 'Month']);

Calendar.propTypes = {
  defaultType: TypePropType,
  type: TypePropType,
  editable: PropTypes.bool,
  navigatable: PropTypes.bool,
  showCurrentTime: PropTypes.bool,
  height: PropTypes.number,
  cellDuration: PropTypes.number,
  startDayHour: PropTypes.number,
  endDayHour: PropTypes.number,
  // eslint-disable-next-line react/forbid-prop-types
  appointmentData: PropTypes.array,
  onChange: PropTypes.func,
};

Calendar.defaultProps = {
  defaultType: 'Week',
  type: undefined,
  editable: true,
  navigatable: true,
  showCurrentTime: true,
  height: undefined,
  cellDuration: 60,
  startDayHour: 6,
  endDayHour: 20,
  onChange: undefined,
  appointmentData: [],
};

export default Calendar;
