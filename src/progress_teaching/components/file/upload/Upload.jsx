/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-unescaped-entities */

import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { useDropzone } from 'react-dropzone';
import { Grid } from '@material-ui/core';
import { toBase64 } from 'Common/utils/Utils';

import classes from './Upload.module.scss';
import FilePreview from '../preview/Preview';

export default function FileUpload(props) {
  const { onChange, disabled } = props;

  const [files, setFiles] = useState([]);
  const [apiFiles, setApiFiles] = useState([]);

  const onDrop = useCallback((acceptedFiles) => {
    setFiles((prev) => [
      ...prev,
      ...acceptedFiles.map((file) => {
        Object.assign(file, {
          previewURL: URL.createObjectURL(file),
        });
        return file;
      }),
    ]);
  }, []);

  const { getRootProps, getInputProps } = useDropzone({ onDrop, disabled });

  useEffect(
    () => () => {
      // revoke the data uris when component unmounts to avoid memory leaks
      files.forEach((file) => URL.revokeObjectURL(file.previewURL));
    },
    [files]
  );

  const createApiFileData = async (file) => {
    const [fileType, binaryData] = `${await toBase64(file)}`.split(';');
    return {
      data: binaryData.split(',')[1],
      fileName: file.name,
      fileType: fileType.split('data:')[1],
    };
  };

  useEffect(() => {
    if (files && files.length > 0)
      Promise.all(files.map(createApiFileData)).then((data) => {
        setApiFiles(data);
      });
  }, [files]);

  useEffect(() => {
    onChange(apiFiles);
  }, [apiFiles]);

  return (
    <Grid item xs={12}>
      <div {...getRootProps()} className={classes.dropzoneContainer}>
        <input {...getInputProps()} />
        <div className={classes.textContainer}>
          <p>Materialien rein ziehen, oder hier klicken</p>
        </div>
      </div>
      {apiFiles.length > 0 && <FilePreview files={apiFiles} />}
    </Grid>
  );
}

FileUpload.propTypes = {
  onChange: PropTypes.func.isRequired,
};
