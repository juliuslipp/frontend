import React, { useState } from 'react';
import ReactFilePreview, { FilePreviewerThumbnail } from 'react-file-previewer';
import { v4 as uuid } from 'uuid';
import { Button, ButtonBase, Divider, Menu, MenuItem, Tooltip } from '@material-ui/core';
import PropTypes from 'prop-types';

import LabelledOutline from 'Teaching/components/labeled_outline/LabelledOutline';
import ProgressDialog from 'Teaching/components/dialog/ProgressDialog';
import classes from './Preview.module.scss';

const uniqueId = uuid();

const initialContextMenuDataState = {
  mouseX: undefined,
  mouseY: undefined,
};

const downloadFile = (file) => {
  if (file) {
    const binaryString = window.atob(file.data);
    const bytes = new Uint8Array(binaryString.length);
    for (let i = 0; i < bytes.length; i += 1) bytes[i] = binaryString.charCodeAt(i);

    const blob = new Blob([bytes], { type: file.fileType });
    const url = URL.createObjectURL(blob);
    const downloadElement = document.createElement('a');
    downloadElement.href = url;
    downloadElement.setAttribute('download', file.fileName);
    downloadElement.click();
    downloadElement.remove();
  }
};

export default function FilePreview(props) {
  const { files } = props;

  const [dialogProps, setDialogProps] = useState({ isOpen: false, file: undefined });
  const [contextMenuData, setContextMenuData] = useState(initialContextMenuDataState);

  const handleContextMenu = (event) => {
    event.preventDefault();
    setContextMenuData(() => ({
      mouseX: event.clientX - 2,
      mouseY: event.clientY - 4,
    }));
  };

  const handleContextMenuClose = () => {
    setContextMenuData(initialContextMenuDataState);
  };

  const dialogContent = dialogProps.file ? (
    <ReactFilePreview
      file={{
        name: dialogProps.file.fileName,
        data: dialogProps.file.data,
        mimeType: dialogProps.file.fileType,
      }}
    />
  ) : (
    <div />
  );

  const dialogActions = (
    <Button variant="text" color="primary" onClick={() => downloadFile(dialogProps.file)}>
      Herunterladen
    </Button>
  );

  return (
    <LabelledOutline id={uniqueId} label="Uploads">
      <ProgressDialog
        title={dialogProps.file && dialogProps.file.fileName}
        onClose={() => {
          setDialogProps({ isOpen: false, file: undefined });
        }}
        open={dialogProps.isOpen}
        content={dialogContent}
        fullScreen
        closeIcon
        actions={dialogActions}
      />

      <div className={classes.previewContainer}>
        {files.map((file) => (
          <React.Fragment key={file.fileName + file.fileType}>
            <ButtonBase
              onClick={() => {
                setDialogProps({
                  isOpen: true,
                  file,
                });
              }}
              onContextMenu={handleContextMenu}
              variant="outlined"
            >
              <div className={classes.contentContainer}>
                <Tooltip title={file.fileName}>
                  <h4 className={classes.title}>{file.fileName}</h4>
                </Tooltip>
                <Divider />
                <FilePreviewerThumbnail
                  file={{
                    name: file.fileName,
                    data: file.data,
                    mimeType: file.fileType,
                  }}
                />
              </div>
            </ButtonBase>
            {contextMenuData.mouseY && contextMenuData.mouseX && (
              <Menu
                keepMounted
                open
                onClose={handleContextMenuClose}
                anchorReference="anchorPosition"
                anchorPosition={{ top: contextMenuData.mouseY, left: contextMenuData.mouseX }}
              >
                <MenuItem
                  onClick={() => {
                    handleContextMenuClose();
                    setDialogProps({
                      isOpen: true,
                      file,
                    });
                  }}
                >
                  Open
                </MenuItem>
                <Divider />
                <MenuItem
                  onClick={() => {
                    handleContextMenuClose();
                    downloadFile(file);
                  }}
                >
                  Download
                </MenuItem>
              </Menu>
            )}
          </React.Fragment>
        ))}
      </div>
    </LabelledOutline>
  );
}

FilePreview.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  files: PropTypes.array.isRequired,
};
