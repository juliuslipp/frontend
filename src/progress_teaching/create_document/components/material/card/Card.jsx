import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Button } from '@material-ui/core';

import useList from 'Common/hooks/useList';
import MaterialForm from 'Teaching/components/document/material/form/Form';
import classes from './Card.module.scss';

export default function DocumentMaterialCard(props) {
  const { onChange } = props;

  const [
    materialList,
    getData,
    addMaterial,
    updateMaterialListAtIndex,
    deleteMaterialListAtIndex,
  ] = useList();

  useEffect(() => {
    onChange(getData());
  }, [materialList]);

  return (
    <div className={classes.formCardContainer}>
      <div className={classes.listContainer}>
        {materialList.map((material, index) => (
          <div key={material.uniqueId} className={classes.formContainer}>
            <div className={classes.form}>
              <MaterialForm onChange={(data) => updateMaterialListAtIndex(data, index)} fullWidth />
            </div>
            <Button color="secondary" onClick={() => deleteMaterialListAtIndex(index)}>
              Materialien löschen
            </Button>
          </div>
        ))}
      </div>
      <Button color="primary" onClick={addMaterial}>
        {materialList.length > 0 ? 'weitere Materialien hinzufügen' : 'Materialien hinzufügen'}
      </Button>
    </div>
  );
}

DocumentMaterialCard.propTypes = { onChange: PropTypes.func.isRequired };
