import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Button } from '@material-ui/core';

import CommentForm from 'Teaching/components/document/comment/form/From';
import useList from 'Common/hooks/useList';
import classes from './Card.module.scss';

export default function CommentCard(props) {
  const { onChange } = props;

  const [
    commentList,
    getData,
    addComment,
    updatePhaseListAtIndex,
    deleteCommentListAtIndex,
  ] = useList();

  useEffect(() => {
    onChange(getData());
  }, [commentList]);

  return (
    <div className={classes.formCardContainer}>
      <div className={classes.listContainer}>
        {commentList.map((comment, index) => (
          <div key={comment.uniqueId} className={classes.formContainer}>
            <div className={classes.form}>
              <CommentForm onChange={(data) => updatePhaseListAtIndex(data, index)} fullWidth />
            </div>
            <Button color="secondary" onClick={() => deleteCommentListAtIndex(index)}>
              Bemerkung löschen
            </Button>
          </div>
        ))}
      </div>
      <Button color="primary" onClick={addComment}>
        {commentList.length > 0 ? 'weitere Bemerkung hinzufügen' : 'Bemerkung hinzufügen'}
      </Button>
    </div>
  );
}

CommentCard.propTypes = { onChange: PropTypes.func.isRequired };
