import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Button } from '@material-ui/core';

import PhaseForm from 'Teaching/components/document/phase/form/Form';
import useList from 'Common/hooks/useList';
import classes from './Card.module.scss';

export default function PhaseCard(props) {
  const { onChange } = props;

  const [phaseList, getData, addPhase, updatePhaseListAtIndex, deletePhaseListAtIndex] = useList();

  useEffect(() => {
    onChange(getData());
  }, [phaseList]);

  return (
    <div className={classes.formCardContainer}>
      <div className={classes.listContainer}>
        {phaseList.map((p, index) => (
          <div key={p.uniqueId} className={classes.formContainer}>
            <div className={classes.form}>
              <PhaseForm onChange={(data) => updatePhaseListAtIndex(data, index)} />
            </div>
            <Button color="secondary" onClick={() => deletePhaseListAtIndex(index)}>
              Phase löschen
            </Button>
          </div>
        ))}
      </div>

      {phaseList.length < 4 ? (
        <Button size="medium" color="primary" onClick={addPhase}>
          {phaseList.length > 0 ? 'Weitere Phase hinzufügen' : 'Phase hinzufügen'}
        </Button>
      ) : null}
    </div>
  );
}

PhaseCard.propTypes = { onChange: PropTypes.func.isRequired };

PhaseCard.defaultProps = {};
