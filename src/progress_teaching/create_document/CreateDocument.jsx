import React, { useState } from 'react';
import { useHistory } from 'react-router';
import { Button } from '@material-ui/core';

import GeneralCard from 'Teaching/components/document/general/form/Form';
import PhaseCard from 'Teaching/create_document/components/phase/card/Card';
import MaterialCard from 'Teaching/create_document/components/material/card/Card';
import DocumentApiService from 'Common/services/DocumentApiService';

import ProgressDialog from 'Teaching/components/dialog/ProgressDialog';
import classes from './CreateDocument.module.scss';
import CommentsCard from './components/comment/card/Card';

export default function CreateDocument() {
  const [generalData, setGeneralData] = useState({});
  const [phasesData, setPhasesData] = useState([]);
  const [materialData, setMaterialData] = useState([]);
  const [commentsData, setCommentsData] = useState([]);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [isSaving, setIsSaving] = useState(false);
  const [error, setError] = useState(undefined);

  const history = useHistory();

  const dialogContent = error ? (
    <p>Beim speichern deines Dokuments ist leider etwas schiefgelaufen!</p>
  ) : (
    <p>Dein Dokument wurde erfolgreich erstellt!</p>
  );
  const dialogActions = (
    <>
      <Button
        variant="text"
        color="primary"
        onClick={() => {
          setDialogOpen(false);
        }}
      >
        Zurück
      </Button>
      {!error && (
        <Button variant="text" color="primary" onClick={() => history.push('/user/profile')}>
          Jetzt ansehen!
        </Button>
      )}
    </>
  );
  const dialogTitle = error
    ? 'Speichern fehlgeschlagen!'
    : `${isSaving ? 'Dokument wird gespiechert...' : 'Dokument gespeichert!'}`;

  const getCardWrapper = (component, title) => (
    <div className={classes.sheetWrapper}>
      <h2 className={classes.sheetHeader}>{title}</h2>
      {component}
    </div>
  );

  const handleButtonClick = () => {
    setError(false);
    setDialogOpen(true);
    setIsSaving(true);
    DocumentApiService.createDocument({
      ...generalData,
      teachingPhaseList: phasesData,
      materialList: materialData,
      commentList: commentsData,
    })
      .catch((err) => {
        setError(err);
      })
      .finally(() => {
        setIsSaving(false);
      });
  };

  return (
    <div>
      <h1 className={classes.pageHeader}>Neues Dokument erstellen</h1>

      {getCardWrapper(
        <GeneralCard onChange={(newData) => setGeneralData(newData)} />,
        'Allgemeine Daten'
      )}
      {getCardWrapper(<PhaseCard onChange={(data) => setPhasesData(data)} />, 'Unterrichtsphasen')}
      {getCardWrapper(<MaterialCard onChange={(data) => setMaterialData(data)} />, 'Materialien')}
      {getCardWrapper(<CommentsCard onChange={(data) => setCommentsData(data)} />, 'Anmerkungen')}

      <div className={classes.buttonContainer}>
        <Button fullWidth className={classes.button} color="primary" onClick={handleButtonClick}>
          Unterrichts-Dokumentation speichern
        </Button>
        <ProgressDialog
          onClose={() => setDialogOpen(false)}
          open={dialogOpen}
          title={dialogTitle}
          loading={isSaving}
          content={dialogContent}
          actions={dialogActions}
          closeIcon
        />
      </div>
    </div>
  );
}
