import { axiosInstance } from 'Common/middleware/Middleware';
import { getClient } from 'Common/services/Utils';

const endpoint = '/calendar';

function CalendarApiService(config = {}) {
  const client = Object.keys(config).length > 0 ? getClient(axiosInstance, config) : axiosInstance;

  return {
    async getCalendar() {
      return client.get(endpoint).then((res) => {
        if (res.data && res.data.appointmentData) {
          const calendar = { ...res.data };
          calendar.appointmentData = JSON.parse(calendar.appointmentData);
          return calendar;
        }
        return res.data;
      });
    },
    async setCalendar(calendar) {
      return client.put(endpoint, calendar);
    },
    async setAppointments(appointmentData) {
      return client.put(`${endpoint}/appointments`, {
        appointmentData: JSON.stringify(appointmentData),
      });
    },
    async resetAppointments() {
      return client.put(`${endpoint}/appointments/reset`);
    },
  };
}

export default CalendarApiService();
