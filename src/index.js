/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import ReactDOM from 'react-dom';
// import { Provider } from 'react-redux';

import './index.scss';
// import store from './common/redux';
import App from './App';

/* 
  Entry point to the App.
  Renders the custom App component
  into the root div inside of index.html.
 */
ReactDOM.render(<App />, document.getElementById('root'));
