/* eslint-disable no-param-reassign */
const webpackConfig = require('./webpack.config.js');

module.exports = function override(config) {
  config.resolve = { ...config.resolve, alias: webpackConfig.resolve.alias };
  config.module.rules[0].parser.requireEnsure = true;
  return config;
};
