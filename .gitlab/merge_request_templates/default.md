# General Consistency

- [ ] Merge Requests link the issue in the header
- [ ] Commit messages are mainly in english and contain the issue number
- [ ] If there is a `TODO`, it references an issue!

# Code Reviewer Commitment

- [ ] Is everything working as expected?
- [ ] Are you satisfied with the overall result?
- [ ] Can you merge the code with a good conscience?